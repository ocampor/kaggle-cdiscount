import numpy as np
import functools

def image2vector(image):
    """
    Argument:
    image -- a numpy array of shape (length, height, depth)

    Returns:
    v -- a vector of shape (length*height*depth, 1)
    """
    size = functools.reduce(lambda x, y: x * y, image.shape)
    return np.reshape(image, (size, 1))


def normalize_image(image):
    return image / 255.
