import collections
import io

import bson
import numpy as np
from bson.codec_options import CodecOptions
from skimage.data import imread

import utils
from utils import normalize_image


class Cdiscount:
    def __init__(self):
        self.train = CdiscountIterator("/media/ocampor/TOSHIBA EXT/cdiscount-image-classification/train.bson")
        self.validation = CdiscountIterator("/media/ocampor/TOSHIBA EXT/cdiscount-image-classification/test.bson",
                                             is_test=True)


class CdiscountIterator:
    def __init__(self, file_name, is_test=False):
        self.bson_file = self._read(file_name)
        self.is_test = is_test

    def _read(self, file_name):
        options = CodecOptions(document_class=collections.OrderedDict)
        file = open(file_name, "rb")
        return bson.decode_file_iter(file, options)

    def next_batch(self, n_samples):
        X = []
        y_ = []
        for _ in range(n_samples):
            sample = next(self.bson_file)
            # If there are no more samples, break
            if sample is None:
                break
            for image in sample["imgs"]:
                image = imread(io.BytesIO(image["picture"]))
                image = normalize_image(image)
                X += [utils.image2vector(image)]
                if self.is_test:
                    y_ += [None]
                else:
                    y_ += [np.array(sample["category_id"])]
        return np.hstack(X), np.vstack(y_)


if __name__ == "__main__":
    data = Cdiscount()
    # When trying to get a batch from test errors
    X, y_ = data.train.next_batch(100)
    print(X.shape)